#Programming skills challenge

Instructions!!! Read with attention!!!
For questions involving code development, use C/C++ or Java syntax to represent the code. The code must be functional and testable.

The result must be sent to a Github / Gitlab or Bitbucket account (aim is to identify familiarity with code versioning systems).

Use at least one file per question and a Readme with comments on how to test and validate your solution or other comments you deem relevant, such as why the file has a particular approach (performance issues, clarity, code maintenance, etc.). Make use of software design patterns that you deem appropriate (eg RAII, Low Coupling, etc.).

For questions related to programming problems using C/C++ or Java, you can use online tools like https://onlinegdb.com to test and validate your solution. For SQL implementation problems, you can use https://sqliteonline.com to test and validate your solution. At the end, provide the link to the tested and validated solution together with the problem response in the repository.

Finally, use the comment field for each question to provide the repository link with your answer.