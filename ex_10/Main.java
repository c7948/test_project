/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
import java.io.*;
import java.util.*;

// Java Program to reverse a string without
// using temp variable
import java.util.*;
 
class Main
{
 
    // Function to reverse string and
    // return reversed string
    static String reversingString(char []str,
                                   int start,
                                   int end)
    {
        // Iterate loop upto start not equal to end
        while (start < end)
        {
            // XOR for swapping the variable
            str[start] ^= str[end];
            str[end] ^= str[start];
            str[start] ^= str[end];
     
            ++start;
            --end;
        }
        return String.valueOf(str);
    }
 
    // Driver Code
    public static void main(String[] args)
    {
        String s = args[0];
        System.out.println(reversingString(s.toCharArray(), 0, s.length()-1));
    }
}
