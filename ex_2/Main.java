/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

Author: Rafael Miggiorin

Function Main: Count from 0 to 100, print Foo if multiple of 3, Baa if multiple of 5, FooBaa if multiple of 3 and 5

*******************************************************************************/
public class Main
{
	public static void main(String[] args) {
		System.out.println("Print Foo if multiple of 3, Baa if multiple of 5, FooBaa if multiple of 3 and 5");
		
		for(int i=0; i<=100;i++)
		{
            if(i%3==0 && i%5==0)
            {
                System.out.println("FooBaa");
            }
            else if(i%3 == 0)
            {
                System.out.println("Foo");
            }
            else if(i%5 == 0)
            {
                System.out.println("Baa");
            }
            else
            {
                System.out.println(i);
            }
		}
	}
}