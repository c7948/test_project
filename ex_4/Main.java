/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/

import java.util.Scanner;
public class Main
{
	public static void main(String[] args) {
        System.out.println("Tests if it is possible to transform given string s into desired string t using a maximum of k operations ");
        
        Scanner myObj = new Scanner(System.in);
        String s, t, opCount;
        
        //Enter reference string and press Enter
        System.out.println("Given string"); 
        s = myObj.nextLine();   
           
        //Enter desired string and press Enter
        System.out.println("Desired string"); 
        t = myObj.nextLine();

        //Enter max operations number and press Enter
        System.out.println("Max operations"); 
        opCount = myObj.nextLine();
        
	    int k = Integer.parseInt(opCount);

        String result = ConcatRemove(s,t,k);
        
        System.out.println(result);
	    
	}
	
	
	/*Verify if testString, or part of it, is inside referenceString*/
	public static int getSubstringIndex(String referenceString, String testString)
	{
	    int size_t = testString.length();
	    int size_r = referenceString.length();

	        
	    for( int i = size_t; i > 0; i--)
	    {
	        String sub_t = testString.substring(0,i);
	        int index_sub_t = referenceString.indexOf(sub_t);
	        System.out.println("Substring t: " + sub_t);
	        System.out.println("Index of substring t in s is: " + index_sub_t);
	        if(index_sub_t == 0)
	        {
	            return sub_t.length()-1;
	        }
	        else if(index_sub_t > 0)
	        {
	            return -1;
	        }
	    }
	    return -1;
	}
	
	public static String ConcatRemove(String s, String t, int k)
	{
	    System.out.println("Strings are: " + s +" "+ t );
	    int size_s = s.length();
	    int size_t = t.length();
	    int matchIndex = getSubstringIndex(s, t);
	    System.out.println("String t in inside String s up to index: " + matchIndex);
	    int reduceCounter =  size_s - (matchIndex+1);
	    int concatCounter =  size_t - (matchIndex+1);
	    System.out.println("Reduce operations = " + reduceCounter);
	    System.out.println("Concat operations = " + concatCounter);
	    if(reduceCounter + concatCounter > k)
	    {
	        return "No";
	    }
	    else
	    {
	        return "Yes";
	    }
	}
}
