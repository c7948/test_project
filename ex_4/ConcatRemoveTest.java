import org.junit.Test;
import static org.junit.Assert.assertEquals;


public class ConcatRemoveTest {

    @Test
    public void testConcatRemove() {
        Main mainTest = new Main();

        String result = mainTest.ConcatRemove("abcde", "fghij", 10);

        assertEquals("Yes", result);

    }
}