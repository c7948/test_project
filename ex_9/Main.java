/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
import java.io.*;
import java.util.*;
public class Main
{
	
	// Get lenght of string
    static int getStringLength(String str)
    {
		int length = 0;
		char[] testArray = str.toCharArray();
        for (char ch: testArray)
        {
            length++;
        }
        return length;
    }
	
	public static void main(String[] args)
	{
		if(args.length == 0)
		{
		    System.out.println("No strings attached");
		    return;
		}
		System.out.println("return length of given string "+args[0]);
		int length = getStringLength(args[0]);
		System.out.println("Tamanho da string é: " + length);
		return;
	}
}